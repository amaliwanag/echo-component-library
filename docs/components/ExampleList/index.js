import React, { PropTypes } from "react"
import Example from "../Example";

const ExampleList = ({ examples }) => {
  return (
    <div>
            {
                examples.map((example) => (
                    <Example key={ example.name } example={example} />
                ))
            }
    </div>
  )
}

ExampleList.propTypes = {
  examples: PropTypes.array.isRequired,
}

export default ExampleList
