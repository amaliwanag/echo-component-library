import React, { PropTypes } from "react"

const Example = ({ example }) => {
    console.log('example', example);
    let ExampleToRender = example.renderer.default;
    console.log('renderer', ExampleToRender);
    return (
        <ExampleToRender />
    );
}

Example.propTypes = {
  example: PropTypes.object.isRequired,
}

export default Example
