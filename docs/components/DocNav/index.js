import React, { PropTypes } from "react"


const DocNav = ({ components }) => {

  return (
    <div className="docs-sidebar" data-spy="affix" data-offset-top="300" data-offset-bottom="200">
        <ol className="intro">       
            <li><a href="#top">Getting Started</a></li>
            {
              components.map((component) => (
                    <li key={component.name}>
                        <a href={`#${component.name}`}>{component.name}</a>
                    </li>
              ))
            }
        </ol>
    </div>
  )
}

DocNav.propTypes = {
  components: PropTypes.array.isRequired,
}

export default DocNav
