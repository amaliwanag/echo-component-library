import React, { PropTypes } from "react"
import ComponentExample from "../ComponentExample";
import styles from "./styles.scss"

const ComponentList = ({ components }) => {
  return (
    <div>
            {
                components.map((component) => (
                    <section key={component.name} id={'#' + component.name} className="section">
                        <div className="row">
                            <div className="col-md-12 left-align">
                                <h2 className="dark-text">{component.name}<a href="#top">#back to top</a><hr /></h2>
                            </div>
                        </div>
                        <ComponentExample component={component}/>
                    </section>
                ))
            }
    </div>
  )
}

ComponentList.propTypes = {
  components: PropTypes.array.isRequired,
}

export default ComponentList
