import React, { PropTypes } from "react"
import ExampleList from '../ExampleList';
import PropsList from '../PropsList';

import AceEditor from 'react-ace';
import brace from 'brace';
import 'brace/theme/solarized_light';

const ComponentExample = ({ component }) => {
    var codeHeight = component.examples[0].source.split('\n').length * 14;
    return (
        <div className="row">
            <div className="col-xs-12">
                <div className="row">
                    <div className="col-xs-6">
                        <ExampleList examples={component.examples} />
                        <PropsList props={component.props} />
                    </div>
                    <div className="col-xs-6">
                        <AceEditor
                            mode="javascript"
                            name={component.name}
                            fontSize={10}
                            theme="solarized_light"
                            value={component.examples[0].source}
                            width='425px'
                            height={codeHeight + 'px'}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

ComponentExample.propTypes = {
  component: PropTypes.object.isRequired,
}

export default ComponentExample
