import React, { PropTypes } from "react"
import { mapValues, values } from 'lodash';

var styles = {
  propText: {
    fontSize: 16
  },
  requiredText: {
    color: 'black'
  },
  enumText: {
    color: '#0CA2D0',
    opacity: '0.8'
  }
};

const PropsList = ({ props }) => {
    let propsArray = mapValues(props, (value, key)=>{
      value["name"] = key;
      return value;
    });

    propsArray = values(propsArray);

  return (
    <div>
        <h2>PropTypes</h2>
        <ul>
          {
              propsArray.map((prop) => (
                  <li key={prop.name}>
                    <strong style={styles.propText}>{prop.name}</strong> - 
                    <em>{prop.type.name}</em> <span style={styles.requiredText}>{prop.required ? "required":""}</span> <span style={styles.enumText}>{prop.type.name === 'enum' ? `[${prop.type.value.map((val) => { return val.value}).toString()}]`:""}</span>
                    <div>
                      {prop.description}
                    </div>
                  </li>
              ))
          }
        </ul>
    </div>
  )
}

PropsList.propTypes = {
  props: PropTypes.object,
}

export default PropsList
