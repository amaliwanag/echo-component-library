import ReactDOM from 'react-dom';
import React from 'react';

//documentation components
import ComponentList from './components/ComponentList';
import DocNav from './components/DocNav';

//markdown
import html from "../README.md";

//auto-generated documentation
import docs from "./docs.js";

var intro = document.getElementById("intro");
intro.innerHTML = html;

ReactDOM.render(<ComponentList components={docs}/>, document.getElementById('ComponentList'));
ReactDOM.render(<DocNav components={docs}/>, document.getElementById('doc-nav'));