# Echo Component Library

Welcome to developing on the Echo Component Library platform. 
Woo this fun!  Don't mess up.

## Getting Started 

To get started developing run `yarn`.

Then run `npm start`.

This will run through `src/components` and create the automated documentation. 
Then it will serve up the documentation site. It will watch the src 
and update as you make changes in the code. However, to update the 
documentation you will have to re-run `npm start`.  

## Directory Stucture

### docs

Contains the documentation static site.

### gulp

Write gulp tasks here.

### icons

Add icons to `icons/svg`

### src/components

Consumed components go here.

### src/containers

Higher order components go here.

### src/utils

Helper modules go here. 


## Adding a components

To add a component simply run `npm run addComp`.  Give your component a name
when prompted. Use PascalCase in the following format. `EchoComponentName`
This will automatically wire it up for auto-documentation. Next time you run
`npm start` you will see your newly create component.

Each component will come with a spec file for tests, an index file for exporting the api
wrapper, a scss file for styles, a main component file, and an example directory for
creating a sample component. Add sub-components to a sub-directory named `/components`.

## Adding documentation.

Add general documentation to the project `README.md` and it will populate to the
static site.  Add proptype documentation by writing comments above the proptypes
in main components like so:

```
    EchoButton.propTypes = {
        /**
        * The text to be displayed in the button.
        */
        label: React.PropTypes.string.isRequired
    }
```

You can associate examples with components by writing a comment above them like so:

```
    /**
    * EchoComponent example
    * 
    * @name SimpleEchoComponentExample
    * @component ../path/to/EchoComponent.js
    */
 ```

 Run `npm run docGen` if you ever want to update the documentation. It spits out
 the result in `/docs/docs.js` which is then consumed by the static site app in
 `/docs/index.js`.

## Testing 

Write your tests in `src/components/EchoComponent/EchoComponent.spec.js`.

Run `npm test`. 

Run `npm test:watch` to re-run the test suite on code changes.

The tests use helper methods from AirBnb's `enzyme` library.
Documentation available at `https://github.com/airbnb/enzyme`;

## Deploying

To deploy the docs and publish the static site. Run `npm run deployDocs`.

Surge will ask you to login the first time you deploy. Credentials are:

```
    email: alex.haines@hotschedules.com
```
```
    password: Bodhi25!
```

The site is deployed at `http://echo-ux.surge.sh/`

To deploy the library run `npm run deploy`.

## Webpack

What the heck is it doing anyway.

### babel-loader

Compiling es6 to es5

### svg-sprite-loader ! svgo-loader

svg-sprite-loader creates sprite sheet for every imported svg and will attach it to </html>
at runtime. Consumers will be able to use icons either through `EchoIcon` or using the
`<use>` tag. svgo-loader optimizes the svgs, removing the un-needed junk.

### style-loader ! css-loader ! postcss-loader ! sass-loader

style-loader loads inline styles. css-loader creates css modules with
hashed class names. postcss-loader adds prefixing for browser compatability
and allows you to write scss selectors with dashes and use camelCase in the
javascript.  sass-loader compiles scss to css. 

### markdown-loader

compiles markdown to html. (used in doc site).





