import React from 'react';
import Styles from './<%= name %>.scss';

/**
 * Enter description.
 * 
 * @name <%= name %>
 */
class <%= name %> extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {
        if (this.theme) {
            //do something with theme
        }

		return (
            <div>Render Awesome Component</div>
        );

    }

}

<%= name %>.defaultProps = {

};

<%= name %>.propTypes = {
    /**
     * Document propTypes like this.
     */
};

export default <%= name %>;