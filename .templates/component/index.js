import React from 'react';
import ReactDOM from 'react-dom';
import <%= name %> from './<%= name %>.js';

class <%= name %>Factory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    <%= name %>.prototype.theme = this.theme;
    return <%= name %>;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<<%= name %> {...props} />, document.querySelector(selector));
  }
}

export let <%= name.charAt(0).toLowerCase() + name.slice(1) %>Factory = new <%= name %>Factory();