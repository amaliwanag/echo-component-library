import React from 'react';
import { <%= name.charAt(0).toLowerCase() + name.slice(1) %>Factory } from '../../<%= name %>';

let <%= name %> =  <%= name.charAt(0).toLowerCase() + name.slice(1) %>Factory.getClass();

/**
 * <%= name %> example
 * 
 * @name <%= name %>Example
 * @component ../../<%= name %>.js
 */
export default class <%= name %>Example extends React.Component {
    render () {
        return (
            <div>
                <<%= name %> />
            </div>
        );
    }
}