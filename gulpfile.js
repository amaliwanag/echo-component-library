(function() {
    "use strict";

    var gulp            = require('gulp')
      , runSequence   = require('run-sequence')
      , requireDir = require('require-dir')
    
    requireDir('./gulp');

     gulp.task('default', function(done) {
        runSequence(
            'buildDocs'
            , 'webpack-dev-server'
            , done
            );
    });

    gulp.task('deploy', function(done) {
        runSequence(
            'cleanDist'
            , ['bundleLibrary', 'copyStaticFilesToDist']
            , 'publish'
            , 'deployDocs'
            , 'bump'
            , done
            );
    });

    gulp.task('deployDocs', function(done) {
        runSequence(
            'buildDocs'
            , 'publishDocs'
            , done
            );
    });

    gulp.task('buildLibrary', function(done) {
        runSequence(
            'cleanDist'
            , ['bundleLibrary', 'copyStaticFilesToDist']
            , done
        );
    });

    gulp.task('buildDocs', function(done) {
        runSequence(
              'docGen'
            , 'bundleDocs'
            , done
        );
    });

})();