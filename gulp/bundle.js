var gulp = require('gulp')
    , webpack = require('webpack')
    , webpackDevServer = require('webpack-dev-server')

gulp.task('bundleLibrary', function (done) {
    webpack(require('../webpack/webpack.config.js'), function () {
        done();
    });
});

gulp.task('bundleDocs', function (done) {
    webpack(require('../webpack/webpack.docs.config.js'), function () {
        done();
    });
});
