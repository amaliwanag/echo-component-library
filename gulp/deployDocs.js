var gulp = require('gulp');
var surge = require('gulp-surge');

gulp.task('publishDocs', [], function () {
  return surge({
    project: './docs',      
    domain: 'echo-ux.surge.sh'
  })
});