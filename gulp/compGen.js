var gulp = require('gulp'),
    template = require('gulp-template'),
    rename = require('gulp-rename'),
    inquirer = require('inquirer');

gulp.task('compGen', (done) => {
    inquirer.prompt([
        { type: 'input', name: 'name', message: 'Give your component a name silly!', default: 'MyComponent' }
    ]).then(function (answers) {
        gulp.src(__dirname + '/../.templates/component/**')
            .pipe(template(answers))
            .pipe(rename(function (file) {
                console.log(file);
                if (file.basename != 'index' && file.extname.length) {
                    file.basename = file.basename.split('.');
                    file.basename[0] = answers.name;
                    file.basename = file.basename.join('.');
                }
            }))
            .pipe(gulp.dest('./src/components/' + answers.name))
            .on('end', function () {
                done();
            })
            .resume();
    });
});