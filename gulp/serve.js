var gulp = require('gulp')
    , webpack = require('webpack')
    , webpackDevServer = require('webpack-dev-server')
    , webpackConfig = require("../webpack/webpack.docs.config.js")

gulp.task("webpack-dev-server", function (callback) {
    var myConfig = Object.create(webpackConfig);
    myConfig.entry.app.unshift("webpack-dev-server/client?http://localhost:8080/", "webpack/hot/dev-server");
    myConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

    new webpackDevServer(webpack(myConfig), {
        stats: {
            colors: true
        },
        hot: true
    }).listen(8080, "localhost");
});