var gulp = require('gulp')
  , shell = require('gulp-shell')

gulp.task('publish', shell.task([
    'cd dist',
    'npm publish',
    'cd ..'
]));