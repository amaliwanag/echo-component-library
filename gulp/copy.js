var gulp = require('gulp');

gulp.task('copyStaticFilesToDist', function () {
    return gulp.src(['./package.json', './README.md'])
        .pipe(gulp.dest('./dist'));
});
