// TODO: generalize the component api tasks


(function () {

    'use strict';
    
    var path = require('path');

    module.exports = [
        {
            entry: {
                Button: './src/components/EchoButton/index.js',
                Drawer: './src/components/EchoDrawer/index.js',
                DropDown: './src/components/EchoDropDown/index.js',
                Row: './src/components/EchoRow/index.js',
                Tree: './src/components/EchoTree/index.js',
                Modal: './src/components/Modal/index.js'
            },
            output: {
                filename: './dist/[name]/index.js',
                library: ['Echo[name]Module'],
                libraryTarget: 'umd'
            },
            module: {
                loaders: [
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        loader: "babel-loader"
                    },
                    {
                        test: /\.svg$/,
                        use: [
                            {
                                loader: "svg-sprite-loader?" + JSON.stringify({
                                    name: 'ico-[name]',
                                    prefixize: true
                                })
                            },
                            {
                                loader: 'svgo-loader',
                                options: {
                                    plugins: [
                                        { removeTitle: true },
                                        { collapseGroups: false },
                                        { removeAttrs: { attrs: 'g:fill' } }
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        test: /\.scss$/,
                        exclude: [
                            /node_modules/,
                            path.resolve(__dirname, "docs/css")
                        ],
                        use: [
                            {
                                loader: 'style-loader'  // creates style nodes from JS strings
                            },
                            {
                                loader: 'css-loader',   // translates CSS into CommonJS
                                options: {
                                    modules: true,
                                    localIdentName: 'echo-component-[name]-[hash:base64:5]'
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    plugins: function () {
                                        return [
                                            require('autoprefixer'),
                                            require('postcss-camel-case')
                                        ];
                                    }
                                }
                            },
                            {
                                loader: "sass-loader"   // compiles Sass to CSS
                            }
                        ]
                    },
                    {
                        test: /\.md$/,
                        use: [
                            {
                                loader: "html-loader"
                            },
                            {
                                loader: "markdown-loader"
                            }
                        ]
                    }
                ]
            }
        }
    ];

})();
