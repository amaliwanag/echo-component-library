(function () {

    'use strict';

    var path = require('path');

    module.exports = {
        name: 'Docs',
        entry: {
            app: ['./docs/index.js']
        },
        output: {
            filename: './docs/app.js',
            library: ['DocsModule'],
            libraryTarget: 'umd'
        },
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader"
                },
                {
                    test: /\.svg$/,
                    use: [
                        {
                            loader: "svg-sprite-loader?" + JSON.stringify({
                                name: 'ico-[name]',
                                prefixize: true
                            })
                        },
                        {
                            loader: 'svgo-loader',
                            options: {
                                plugins: [
                                    { removeTitle: true },
                                    { collapseGroups: false},
                                    { removeAttrs: {attrs: 'g:fill'}}
                                ]
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    exclude: [
                        /node_modules/,
                        path.resolve(__dirname, "docs/css")
                    ],
                    use: [
                        {
                            loader: 'style-loader'  // creates style nodes from JS strings
                        },
                        {
                            loader: 'css-loader',   // translates CSS into CommonJS
                            options: {
                                modules: true,
                                localIdentName: 'echo-component-[name]-[hash:base64:5]'
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        require('autoprefixer'),
                                        require('postcss-camel-case')
                                    ];
                                }
                            }
                        },
                        {
                            loader: "sass-loader"   // compiles Sass to CSS
                        }
                    ]
                },
                {
                    test: /\.md$/,
                    use: [
                        {
                            loader: "html-loader"
                        },
                        {
                            loader: "markdown-loader"
                        }
                    ]
                }
            ]
        },
        plugins: []
    };

})();
