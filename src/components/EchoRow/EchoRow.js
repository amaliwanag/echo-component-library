import React from 'react';
import Styles from './EchoRow.scss';
import { isArray, isString } from 'lodash';

/**
 * Enter description.
 * 
 * @name EchoRow
 */
class EchoRow extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const rowClasses = [
            Styles.row,
            Styles[this.props.media],
            this.props.noPadding ? Styles.noPad : null
        ];

        const rowStyles = {
            justifyContent  : this.props.justify ? this.props.justify:"flex-start"
        };

		return (
            <div style={rowStyles} className={rowClasses.join(" ")}>
                {   
                    React.Children.map(this.props.children, (child, index) => {
                        let colStyles = {};

                        if (this.props.columns && this.props.columns[index]) {
                            let property = isString(this.props.columns[index]) ? 'flexBasis':'flexGrow';
                            colStyles[property] = this.props.columns[index] || 1;
                        } else if (!this.props.justify) {
                            colStyles.flexGrow = 1;
                        }

                        return <div key={index} style={colStyles} className={Styles.col}>{child}</div>;
                    })
                }
            </div>
        );

    }

}

EchoRow.defaultProps = {
    
};

EchoRow.propTypes = {
    /**
     * Used to define the size ratio by which columns relate to each other
     * i.e. [3,1] - Column one will be 75% of the width, while column two will be 25%.
     * A column can also be set to a fixed with via string. i.e. ['30px', 1] - Column
     * one will be 30px wide while column two will stretch to fit the remaining space.
     */
     columns: React.PropTypes.array,
     /**
     * Used to justify the children of the row. See flexbox docs on 'justify-content' for more detail.
     * Cannot be used in conjuction with columns as columns expand to fill the width of the parent.
     */
     justify: React.PropTypes.oneOf(['flex-start', 'flex-end', 'center', 'space-between', 'space-around']),
     /**
     * The point at which the rows stack when the window is being resized. 
     * The breakpoints are 1024px, 768px, 480px, 300px respectively.
     */
     media: React.PropTypes.oneOf(['tablet-landscape', 'tablet-portrait', 'phone-lanscape', 'phone-portrait']),
     /**
     * Remove row outer padding.
     */
     noPadding: React.PropTypes.bool
};

export default EchoRow;