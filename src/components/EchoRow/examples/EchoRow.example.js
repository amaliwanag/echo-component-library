import React from 'react';
import Styles from './EchoRow.example.scss';

import { echoRowFactory } from '../../EchoRow';
import { echoButtonFactory } from '../../EchoButton';

let EchoButton  = echoButtonFactory.getClass();
let EchoRow     =  echoRowFactory.getClass();

/**
 * EchoRow example
 * 
 * @name EchoRowExample
 * @component ../../EchoRow.js
 */
export default class EchoRowExample extends React.Component {
    render () {
        return (
            <div className={Styles.container}>
                <p className={Styles.rowLabel}>
                    Simple Two Column Row
                </p>
                <EchoRow>
                    <div className={Styles.box}></div>
                    <div className={Styles.box}></div>
                </EchoRow>

                <p className={Styles.rowLabel}>
                    Nested Row
                </p>
                <EchoRow>
                    <div className={Styles.box}></div>
                    <EchoRow>
                        <div className={Styles.box}></div>
                        <div className={Styles.box}></div>
                    </EchoRow>
                </EchoRow>

                <p className={Styles.rowLabel}>
                    Row with column size ratios defined
                </p>
                <EchoRow columns={[3,1]} media="tablet-portrait">
                    <div className={Styles.box}></div>
                    <div className={Styles.box}></div>
                </EchoRow>

                <p className={Styles.rowLabel}>
                    Row with justified child
                </p>
                <EchoRow justify="center">
                    <EchoButton label="Centered Button" />
                </EchoRow>
            </div>
        );
    }
}