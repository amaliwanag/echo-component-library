import React from 'react';
import ReactDOM from 'react-dom';
import EchoRow from './EchoRow.js';

class EchoRowFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoRow.prototype.theme = this.theme;
    return EchoRow;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoRow {...props} />, document.querySelector(selector));
  }
}

export let echoRowFactory = new EchoRowFactory();