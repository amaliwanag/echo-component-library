import React from 'react';
import ReactDOM from 'react-dom';
import EchoTree from './EchoTree.js';

class EchoTreeFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoTree.prototype.theme = this.theme;
    return EchoTree;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoTree {...props} />, document.querySelector(selector));
  }
}

export let echoTreeFactory = new EchoTreeFactory();