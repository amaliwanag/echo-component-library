import React from 'react';
import Styles from './EchoTree.scss';

//animations
import makeCollapse from '~/src/containers/animations/makeCollapse.js';

//child components
import ListItem from './components/ListItem';
const CollapsingListItem = makeCollapse(ListItem, {duration: 0.4});

/**
 * Enter description.
 * 
 * @name EchoDropDown
 */
class EchoTree extends React.Component {

    constructor(props) {
        super(props);

    }

    componentWillReceiveProps(nextProps) {
        console.log('nextprops!', nextProps.collection);
        console.log('state!', this.state.collection);
        if (JSON.stringify(nextProps.collection) != JSON.stringify(this.state.collection)) {
            console.log('setstate!');
            this.setState({
                collection: nextProps.collection
            })
        }
    }

    componentWillMount() {
        this.setState({
            collection: this.props.collection
        })
    }

    renderListItems(collection) {
        return (
            collection.map((item, index)=> {
                return <ListItem 
                            key={index}
                            item={item}
                            height={this.props.lineHeight}
                        />
            })
        );
    }

    render() {
        let style = {
            backgroundColor: 'red',
            lineHeight: this.props.lineHeight ? `${this.props.lineHeight}px`:'36px'
        }

		return (
            <div className={Styles.treeContainer} >
                <ul style={style} className={Styles.tree}>
                    {this.renderListItems(this.state.collection)}                    
                </ul>
            </div>
        );

    }

}

EchoTree.defaultProps = {

};

EchoTree.propTypes = {
    /**
     * The collection of objects that will create the hierarchy. Object
     * properties are label, onClick, action, icon, template, and children.  Label is the string to 
     * be displayed on the dropdown list item. OnClick is the function to be called when
     * the item is clicked.  Action is an object that defines a secondary action available
     * to that line item; it takes an onClick property and a icon property. Icon is an icon 
     * object that will display an icon before the item label. Template takes a string which 
     * corresponds to a predifined template or a function that returns JSX. Predefined templates
     * include 'section-break'. Children is an array of sub-item objects.
     * 
     */
    collection: React.PropTypes.array.isRequired,
    /**
     * set the line height for each item.
     * 
     */
    itemHeight: React.PropTypes.number
};

export default EchoTree;