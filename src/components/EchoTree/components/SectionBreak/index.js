import React from 'react';
import styles from './index.scss';

import { echoIconFactory } from '~/src/components/EchoIcon';
const EchoIcon = echoIconFactory.getClass();



const SectionBreak = ({item}) => {
    return (
        <div style={{lineHeight: '36px'}} className={styles.sectionBreak}>
            {item.label}
            <span style={{paddingLeft: 5}}>
                <EchoIcon onClick={item.onClick} name={item.icon.name} height={item.icon.height || 20} width={item.icon.width || 20} scale={item.icon.scale} color={item.icon.color} />
            </span>
        </div>
    );
};


SectionBreak.defaultProps = {
    animationStyles: {start: {}, end: {}}
};

export default SectionBreak;