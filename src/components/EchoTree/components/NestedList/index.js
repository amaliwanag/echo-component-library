import React from 'react';
import Styles from './index.scss';

//utils
import colors from '~/src/utils/colors.js';

//child components
import ListItem from '../ListItem';

const NestedList = ({items, height}) => {
    return (
        <ul className={Styles.nestedDropdown}>
            { 
                items.map((item, index)=> {
                    return <ListItem 
                                key={index}
                                item={item}
                                height={height}
                            />;
                })
            }
        </ul>
    );
};


NestedList.defaultProps = {
    animationStyles: {start: {}, end: {}}
};

export default NestedList;