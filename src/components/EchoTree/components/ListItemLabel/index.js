import React from 'react';
import styles from './index.scss';

//animations 
import makeRotate from '~/src/containers/animations/makeRotate.js';

//sub-components
import NestedList from '../NestedList';
import { echoIconFactory } from '~/src/components/EchoIcon';
import { echoRowFactory } from '~/src/components/EchoRow';

//create instance of rotating icon
const EchoIcon = echoIconFactory.getClass();
const RotatingIcon = makeRotate(EchoIcon, {degrees: -90, duration: 0.3});
const EchoRow = echoRowFactory.getClass();

/**
 * Renders item label.
 * Will render a caret icon if the item has children.
 * Will render a action button with custom icon if action is present.
 */
const ListItemLabel = ({item, onClick, onItemClick, height, isOpen}) => {
    let hasChildren     = item.children && item.children.length;
    let hasAction       = item.action && item.action.onClick; 
    let controlTemplate = null;

    if (hasChildren) {
        controlTemplate = (
            <div style={{width: height}} className={styles.listItemControl} onClick={onClick}>
                <RotatingIcon color="$light-gray" name="ico-chevron-left" width={36} isAnimated={isOpen} />
            </div>
        )
    } else if (hasAction) {
        controlTemplate = (
            <div  style={{width: height}} className={styles.listItemControl} onClick={item.action.onClick.bind(null, item)}>
                {
                    item.action && 
                        <EchoIcon name={item.action.icon.name} height={36} width={36} scale={item.action.icon.scale} color={item.action.icon.color} />
                }
            </div>
        )
    }

    let classes = [
        styles.labelContainer,
        item.isActive ? styles.active:null
    ]

    return (
        <div className={classes.join(" ")}>
            <div style={{marginLeft: item.icon ? -10:0}} className={styles.listItemLabel} onClick={onItemClick.bind(null, item)}>
                        {
                            item.icon && 
                                <EchoIcon name={item.icon.name} height={item.icon.height || 36} width={item.icon.width || 36} scale={item.icon.scale} color={item.icon.color} />
                        }
                    <span>{item.label}</span>
            </div>
            {controlTemplate}
        </div>
    );
};

export default ListItemLabel;