import React from 'react';
import Styles from './index.scss';

//animations 
import makeCollapse from '~/src/containers/animations/makeCollapse.js';

//sub-components
import NestedList from '../NestedList';
import ListItemLabel from '../ListItemLabel';
import SectionBreak from '../SectionBreak';
const CollapsingNestedList = makeCollapse(NestedList);


const listItemTemplates = {
    'section-break': SectionBreak
};

class ListItem extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.handleClick        = this.handleClick.bind(this);
        this.handleItemClick        = this.handleItemClick.bind(this);
    }

    handleItemClick(item) {
        if (item && item.onClick) {
            item.onClick(item);
        }
 
        this.setState({item: item});
    }

    handleClick() {
        this.setState({isOpen: !this.state.isOpen});
    }
    
    componentWillMount() {
        this.setState({
            item: this.props.item
        });
    }

    render() {

        let classes = [
            Styles.listItem,
            this.state.item.isActive ? Styles.active:null
        ]

		return (
            <li className={classes.join(" ")}>
                {   
                    //render custom or predifined template or default
                    this.props.item.template
                        ? listItemTemplates[this.state.item.template]({item: this.state.item}) || this.props.item.template(this.state.item)
                        : <ListItemLabel item={this.state.item} height={this.state.height} onClick={this.handleClick} onItemClick={this.handleItemClick} isOpen={this.state.isOpen} />
                }
                {    
                    //render subnav if item has children
                    this.state.item.children && this.state.item.children.length 
                        ? <CollapsingNestedList isAnimated={this.state.isOpen} items={this.state.item.children} height={this.state.height}  />
                        : null
                }
            </li>
        );

    }

}

ListItem.defaultProps = {

};

ListItem.propTypes = {
    item: React.PropTypes.object
};

export default ListItem;