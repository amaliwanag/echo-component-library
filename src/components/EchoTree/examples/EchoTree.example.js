import React from 'react';
import { echoTreeFactory } from '../../EchoTree';

let EchoTree = echoTreeFactory.getClass();

const collection = [
    {
        label: 'nav one',
        isActive: false,
        onClick: (item) => { item.isActive = !item.isActive; console.log('item!', item); },
        children: [
            {
                label: 'sub nav one'
            },
            {
                label: 'sub nav two',
                children: [
                    {
                        label: 'deep nav one'
                    },
                    {
                        label: 'deep nav two'
                    }
                ]
            }
        ]
    },
    {
        label: 'this is a longer destination name',
        children: [
            {
                label: 'child with action',
                onClick: function () { alert('I have two actions.') },
                action: {
                    icon: {
                        name: "ico-plus",
                        scale: 1.3,
                        color: "$dark-blue"
                    },
                    onClick: () => { alert('perform action') }
                }
            },
            {
                label: 'child two',
                children: [
                    {
                        label: 'sub child'
                    },
                    {
                        label: 'sub child'
                    }
                ]
            }
        ]
    }
];

/**
 * EchoTree example
 * 
 * @name EchoTreeExample
 * @component ../../EchoTree.js
 */
export default class EchoTreeExample extends React.Component {
    render() {
        return (
            <div style={{ height: 300, border: '1px solid #ccc', overflow: 'scroll' }}>
                <EchoTree collection={collection} />
            </div>
        );
    }
}