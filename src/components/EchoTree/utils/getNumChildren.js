function countChildren(currentChild) {
    var count = 0;

    if (currentChild.children) {
        count = currentChild.children.length;

        for (var i = 0; i < currentChild.children.length; i++) {
            var child = currentChild.children[i];
            count += countChildren(child);
        };
    }

    return count;
};

export default function (children) {
    var originChild = {
        children: children
    }

    return countChildren(originChild);
}