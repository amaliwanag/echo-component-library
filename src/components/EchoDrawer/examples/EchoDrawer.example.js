import React from 'react';
import { echoDrawerFactory } from '../../EchoDrawer';
import { echoButtonFactory } from '../../EchoButton';

let EchoDrawer = echoDrawerFactory.getClass();
let EchoButton = echoButtonFactory.getClass();

const navItems = [
    {
        label: 'Home',
        children: [
            {
                label: 'Dashboard'
            },
            {
                label: 'Feed'
            }
        ]
    },
    {
        label: 'Staff',
        children: [
            {
                label: 'Staff List'
            },
            {
                label: 'Staff List',
                children: [
                    {
                        label: 'Staff Cert'
                    },
                    {
                        label: 'Staff Journal'
                    }
                ]
            }
        ]
    }
];

const accountItems = [
    {
        label: 'My Account',
        icon: {
            name: "ico-human",
            color: "$dark-gray"
        }
    },
    {
        label: 'Settings',
        icon: {
            name: "ico-settings",
            color: "$dark-gray"
        }
    },
    {   
        label: 'Add Module',
        template: 'section-break',
        icon: {
             name: "ico-help-filled"
        },
        onClick: () => { alert('get help...') }
    },
    {
        label: 'Production',
        action: {
            icon: {
                name: 'ico-plus'
            },
            onClick: () => { alert('add module...') }
        }
    },
    {
        label: 'Recipe',
        action: {
            icon: {
                name: 'ico-plus'
            },
            onClick: () => { alert('add module...') }
        }
    },
    {
        label: 'Vendors',
        action: {
            icon: {
                name: 'ico-plus'
            },
            onClick: () => { alert('add module...') }
        }
    }
];

/**
 * EchoDrawer example
 * 
 * @name EchoDrawerExample
 * @component ../../EchoDrawer.js
 */
export default class EchoDrawerExample extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({isOpen: !this.state.isOpen});
    }
    
    render() {
        return (
            <div>
                <EchoButton label="toggle drawer" onClick={this.handleClick} />
                <EchoDrawer isOpen={this.state.isOpen} onClick={this.handleClick} navItems={navItems} accountItems={accountItems} />
            </div>
        );
    }
}