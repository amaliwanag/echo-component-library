import React from 'react';
import styles from './index.scss';

//animations
const Shade = ({onClick, isAnimated, onAnimate, animationStyles}) => {
    let style = {};

    if (animationStyles && onAnimate) {
        style = Object.assign(style, animationStyles.start);
        style = isAnimated ? Object.assign(style, animationStyles.end):style;
    }

    return (
        <div onClick={onClick} style={style} className={styles.shade}></div>
    );
};

export default Shade;