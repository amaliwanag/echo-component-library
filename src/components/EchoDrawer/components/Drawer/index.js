import React from 'react';
import styles from './index.scss';

//sub-components
import { echoRowFactory } from '~/src/components/EchoRow';
import { echoTreeFactory } from '~/src/components/EchoTree';
import { echoIconFactory } from '~/src/components/EchoIcon';

const EchoRow   = echoRowFactory.getClass();
const EchoTree  = echoTreeFactory.getClass();
const EchoIcon  = echoIconFactory.getClass();

const Drawer = ({navItems, accountItems, onClick, isAnimated, onAnimate, animationStyles}) => {
    let style = {};

    if (animationStyles && onAnimate) {
        style = Object.assign(style, animationStyles.start);
        style = isAnimated ? Object.assign(style, animationStyles.end):style;
    }

    return (
        <div style={style} className={styles.drawer}>
            <div className={styles.header}>
                <EchoRow columns={['45px', 1]} noPadding> 
                    <div className={styles.close} onClick={onClick}>
                        <EchoIcon name="ico-close" width={36} color="$dark-gray" />
                    </div>
                    <div className={styles.logo}>
                        <EchoIcon name="ico-hslogo_mono" width={120} height={30} color="$blue" />
                    </div>
                </EchoRow>
            </div>
            <EchoTree collection={navItems} lineHeight={45} />
            <div className={styles.divider}></div>
            <EchoTree collection={accountItems} lineHeight={45} />
        </div>
    );
};

export default Drawer;