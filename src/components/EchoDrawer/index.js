import React from 'react';
import ReactDOM from 'react-dom';
import EchoDrawer from './EchoDrawer.js';

class EchoDrawerFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoDrawer.prototype.theme = this.theme;
    return EchoDrawer;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoDrawer {...props} />, document.querySelector(selector));
  }
}

export let echoDrawerFactory = new EchoDrawerFactory();