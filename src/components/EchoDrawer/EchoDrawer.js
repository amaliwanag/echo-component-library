import React from 'react';
import Styles from './EchoDrawer.scss';

//animations
import makeMove from '~/src/containers/animations/makeMove.js';
import makeFade from '~/src/containers/animations/makeFade.js';

//sub-components
import Drawer from './components/Drawer';
import Shade from './components/Shade';

const MovingDrawer  = makeMove(Drawer, {  xEnd: '0', xStart: '-101%', duration: 0.35 });
const FadingShade   = makeFade(Shade, {  endOpacity: 0.65, duration: 0.35 });

/**
 * Enter description.
 * 
 * @name EchoDrawer
 */
class EchoDrawer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.closeDrawer = this.closeDrawer.bind(this);
        this.openDrawer = this.openDrawer.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isOpen != this.state.isOpen) {
            nextProps.isOpen ? this.openDrawer() : this.closeDrawer();
        }
    }

    openDrawer() {
        this.setState({isOpen: true});
    }

    closeDrawer() {
        this.setState({isOpen: false});
    }

    render() {
        if (this.theme) {
            //do something with theme
        }

        return (
            <div>
                <MovingDrawer isAnimated={this.state.isOpen} onClick={this.props.onClick} navItems={this.props.navItems} accountItems={this.props.accountItems} />
                <FadingShade isAnimated={this.state.isOpen} onClick={this.props.onClick} />
            </div>
        );

    }

}

EchoDrawer.defaultProps = {

};

EchoDrawer.propTypes = {
    /**
     * The collection of objects that will create the hierarchy. Object
     * properties are label, onClick, action, icon, template, and children.  Label is the string to 
     * be displayed on the dropdown list item. OnClick is the function to be called when
     * the item is clicked.  Action is an object that defines a secondary action available
     * to that line item; it takes an onClick property and a icon property. Icon is an icon 
     * object that will display an icon before the item label. Template takes a string which 
     * corresponds to a predifined template or a function that returns JSX. Predefined templates
     * include 'section-break'. Children is an array of sub-item objects.
     * 
     */
    navItems: React.PropTypes.array.isRequired,
    /**
     * Same as navItems. But will be displayed in the lower
     * section of the drawer. Used for options concerning 
     * the user's settings.  
     * 
     */
    accountItems: React.PropTypes.array,
    /**
     * Toggles the open/closed state of the drawer
     */
    isOpen: React.PropTypes.bool.isRequired,
    /**
     * A function that controls the open/closed state of the drawer.
     * Used for closing the drawer. 
     */
    onClick: React.PropTypes.func.isRequired
};

export default EchoDrawer;