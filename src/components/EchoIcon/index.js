import React from 'react';
import ReactDOM from 'react-dom';
import EchoIcon from './EchoIcon.js';

class EchoIconFactory {
  constructor() {
  }

  getClass() {
    return EchoIcon;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoIcon {...props} />, document.querySelector(selector));
  }
}

export let echoIconFactory = new EchoIconFactory();