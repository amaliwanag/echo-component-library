import React from 'react';
import styles from './EchoIcon.scss';

//util
import colors from '~/src/utils/colors.js';

//import svg lib
var files = require.context('../../../icons/svg', false, /\.svg$/);
files.keys().forEach(files);


const EchoIcon = ({name, color, scale, width, height, onClick, isAnimated, onAnimate, animationStyles}) => {

  let styles = {
    transform: "",
    verticalAlign: 'middle'
  }

  //apply animation if present
  if (animationStyles) {
    styles = Object.assign(styles, animationStyles.start);
    styles = isAnimated ? Object.assign(styles, animationStyles.end):styles;
  }

  //scale if present
  scale ? styles.transform += ` scale(${scale})`:null;

  function handleClick(e) {
    onAnimate && onAnimate(e);
    onClick && onClick(e);
  }

  return (
    <svg style={styles} width={width || height || 32} height={height || width || 32} fill={colors[color] || color} onClick={handleClick}>
      <use xlinkHref={`#${name}`} />
    </svg>
  );
};

EchoIcon.propTypes = {
    /**
     * The name of the icon. See http://shaunfox.com/work/fox-icons.html 
     * for full list of icons.
     */
    name: React.PropTypes.string.isRequired,
    /**
     * Width of icon. Default is 32px. Height will match width
     * if not specified.
     */
    width: React.PropTypes.number,
    /**
     * Height of icon. Default is 32px. Width will match height
     * if not specified.
     */
    height: React.PropTypes.number,
    /**
     * Transform the scale of the icon. Just another way to size. 
     */
    scale: React.PropTypes.number,
    /**
     * Color of icon excepts hex-colors or sass variables. i.e. $gray
     * Defaults to $blue.
     */
    color: React.PropTypes.string,
    /**
     * Function to be called on click event.
     */
    onClick: React.PropTypes.func
};

EchoIcon.defaultProps = {
  color: "$blue"
};

export default EchoIcon;