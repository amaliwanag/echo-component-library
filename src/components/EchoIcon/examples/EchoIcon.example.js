import React from 'react';
import { echoIconFactory } from '../../EchoIcon';

let EchoIcon =  echoIconFactory.getClass();

/**
 * EchoIcon example
 * 
 * @name EchoIconExample
 * @component ../../EchoIcon.js
 */
export default class EchoIconExample extends React.Component {
    render () {
        return (
            <div>
                <EchoIcon name="ico-baseball-card" />
                <EchoIcon name="ico-bug" color="$red" />
                <EchoIcon name="ico-camera" color="$green" />
                <EchoIcon name="ico-comment" width={50} />
                <EchoIcon name="ico-database" scale={2} />
                <EchoIcon name="ico-earth" onClick={(()=> {alert('hey there')})} />
                <EchoIcon name="ico-inbox" color="$orange" />
                <EchoIcon name="ico-mail-alt" color="$dark-blue" />
                <EchoIcon name="ico-picture" />
            </div>
        );
    }
}