import React from 'react';
import styles from './EchoDateRange.scss';
import moment from 'moment';

import { echoIconFactory } from '../EchoIcon';

const EchoIcon = echoIconFactory.getClass();

/**
 * Enter description.
 * 
 * @name EchoDateRange
 */
class EchoDateRange extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            startDate: null,
            endDate: null
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.startDate != this.state.startDate || nextProps.endDate != this.state.endDate ) {
            this.setState({startDate: nextProps.startDate, endDate: nextProps.endDate})
        }
    }

    renderRange(dateOne, dateTwo) {
        let format    = this.props.showTime ?  'ddd M/D/YY h:mm:ss a' : 'ddd M/D/YY';

        return (
            <div className={styles.dateText}>
                { moment(new Date(dateOne)).format(format) } -  {  moment(new Date(dateTwo)).format(format) }
            </div>
        );
    }

    render() {
		return (
            <div className={styles.dateRange}>
                {
                   this.props.onDecrement && <EchoIcon onClick={this.props.onDecrement} name="ico-chevron-left" width={20} color="$gray" />
                }
                {
                    this.renderRange(this.props.startDate, this.props.endDate)
                }
                {
                   this.props.onIncrement && <EchoIcon onClick={this.props.onIncrement} name="ico-chevron-right" width={20} color="$gray" />
                }
            </div>
        );

    }

}

EchoDateRange.defaultProps = {
    showTime: false
};

EchoDateRange.propTypes = {
    /**
     * Start of date range. Can be a string or a Date object.
     */
    startDate: React.PropTypes.oneOfType(['string', 'object']).isRequired,
    /**
     * End of date range. Can be a string or a Date object.
     */
    endDate: React.PropTypes.oneOfType(['string', 'object']).isRequired,
    /**
     * Function to increment date range. If left blank arrows will not be displayed.
     */
    onIncrement: React.PropTypes.func.isRequired,
    /**
     * Function to decrement date range. If left blank arrows will not be displayed.
     */
    onDecrement: React.PropTypes.func.isRequired,
    /**
     * Toggles whether or not time is shown in addition to date.
     * i.e. 3:35:34 pm
     */
    showTime: React.PropTypes.bool
    
};

export default EchoDateRange;