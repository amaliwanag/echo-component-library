import React from 'react';
import { echoDateRangeFactory } from '../../EchoDateRange';
import { echoRowFactory } from '../../EchoRow';
import moment from 'moment';

let EchoDateRange =  echoDateRangeFactory.getClass();
let EchoRow =  echoRowFactory.getClass();

/**
 * EchoDateRange example
 * 
 * @name EchoDateRangeExample
 * @component ../../EchoDateRange.js
 */
export default class EchoDateRangeExample extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: new Date("2016-04-01T00:00:00.066Z"),
            endDate: new Date("2016-04-08T00:00:00.066Z")
        };

        this.handleIncrement = this.handleIncrement.bind(this);
        this.handleDecrement = this.handleDecrement.bind(this);
    }
    
    handleIncrement() {
        this.setState({
            startDate: moment(this.state.startDate).add(7, 'days').toISOString(),
            endDate: moment(this.state.endDate).add(7, 'day').toISOString()
        });
    }

    handleDecrement() {
        this.setState({
            startDate: moment(this.state.startDate).subtract(7, 'days').toISOString(),
            endDate: moment(this.state.endDate).subtract(7, 'days').toISOString()
        });
    }

    render () {
        return (
            <EchoRow justify="center">
                <EchoDateRange 
                    startDate={this.state.startDate} 
                    endDate={this.state.endDate}
                    showTime={false}
                    onIncrement={this.handleIncrement}
                    onDecrement={this.handleDecrement}
                />
            </EchoRow>
        );
    }
}