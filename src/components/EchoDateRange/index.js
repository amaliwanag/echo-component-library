import React from 'react';
import ReactDOM from 'react-dom';
import EchoDateRange from './EchoDateRange.js';

class EchoDateRangeFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoDateRange.prototype.theme = this.theme;
    return EchoDateRange;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoDateRange {...props} />, document.querySelector(selector));
  }
}

export let echoDateRangeFactory = new EchoDateRangeFactory();