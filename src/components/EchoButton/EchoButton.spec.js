import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import Button from './EchoButton.js';

describe("A suite", function() {
    it("contains spec with an expectation", function() {
        expect(false).to.equal(true);
    });
    
    xit("it renders a button tag", function() {
        expect(shallow(<Button />).contains(<button />)).to.equal(true);
    });

    xit("contains spec with an expectation", function() {
        expect(shallow(<Foo />).is('.foo')).to.equal(true);
    });

    xit("contains spec with an expectation", function() {
        expect(mount(<Foo />).find('.foo').length).to.equal(1);
    });
});