import React from 'react';
import ReactDOM from 'react-dom';
import EchoButton from './EchoButton.js';

class ButtonFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoButton.prototype.theme = this.theme;
    return EchoButton;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoButton {...props} />, document.querySelector(selector));
  }
}

export let echoButtonFactory = new ButtonFactory();