import React from 'react';
import { echoButtonFactory } from '../../EchoButton';

let EchoButton =  echoButtonFactory.getClass();

/**
 * Button example
 * 
 * @name SimpleButtonExample
 * @component ../../EchoButton.js
 */
export default class ButtonExample extends React.Component {
    handleClick() {
        alert('Clicked!');
    }
    render () {
        return (
            <div>
                <EchoButton 
                    label="Submit"
                    onClick={this.handleClick}
                />
                <EchoButton 
                    label="Submit"
                    size="large"
                    onClick={this.handleClick}
                />
                <EchoButton 
                    label="Submit"
                    type="secondary"
                    onClick={this.handleClick}
                />
                <EchoButton 
                    label="Submit"
                    type="secondary"
                    size="large"
                    onClick={this.handleClick}
                    isDisabled={true}
                />
            </div>
        );
    }
}