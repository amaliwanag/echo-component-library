import React from 'react';
import Styles from './EchoButton.scss';

/**
 * A simple button.
 * 
 * @name EchoButton
 */
class EchoButton extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isDisabled: props.isDisabled || false
        };
    }

	componentWillReceiveProps(props) {
        this.setState({
            isDisabled: props.isDisabled
        });
    }

    render() {
        var classes = [
            Styles.button,
            this.props.type === 'secondary' ? Styles.secondary : '',
            this.props.size === 'large' ? Styles.large : ''
        ];

        let style = {
            whiteSpace: 'nowrap'
        };

        if (this.theme) {
            style.background = this.theme.primary_color
        }

		return (
            <button  
				style={style}
                className={classes.join(' ')} 
                onClick={this.props.onClick}
                disabled={this.state.isDisabled}
            >{this.props.label}</button>
        );

    }

}

EchoButton.defaultProps = {
	type: 'primary'
};

EchoButton.propTypes = {
    /**
     * The text to be displayed in the button.
     */
    label: React.PropTypes.string.isRequired,
    /**
     *  Controls the contrasting color scheme of the button.
     */
    type: React.PropTypes.oneOf(['primary', 'secondary']),
    /**
     *  Controls the size of the button.
     */
    size: React.PropTypes.oneOf(['large']),
    /**
     *  Function to be executed on click event.
     */
    onClick: React.PropTypes.func,
    /**
     *  Toggles the disabled state of button.
     */
    isDisabled: React.PropTypes.bool,
    /**
     *  Used for overriding select styles.
     */
	theme: React.PropTypes.object
};

export default EchoButton;