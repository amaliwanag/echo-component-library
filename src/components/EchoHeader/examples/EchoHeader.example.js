import React from 'react';
import { echoHeaderFactory } from '../../EchoHeader';
import { echoButtonFactory } from '../../EchoButton';

let EchoHeader =  echoHeaderFactory.getClass();
let EchoButton =  echoButtonFactory.getClass();

const navItems = [
    {
        label: 'Home',
        isActive: true,
        children: [
            {
                label: 'Dashboard'
            },
            {
                label: 'Feed'
            }
        ]
    },
    {
        label: 'Staff',
        children: [
            {
                label: 'Staff List'
            },
            {
                label: 'Staff List',
                children: [
                    {
                        label: 'Staff Cert'
                    },
                    {
                        label: 'Staff Journal'
                    }
                ]
            }
        ]
    }
];

const accountItems = [
    {
        label: 'My Account',
        icon: {
            name: "ico-human",
            color: "$dark-gray"
        }
    },
    {
        label: 'Settings',
        icon: {
            name: "ico-settings",
            color: "$dark-gray"
        }
    },
    {   
        label: 'Add Module',
        template: 'section-break',
        icon: {
             name: "ico-help-filled"
        },
        onClick: () => { alert('get help...') }
    },
    {
        label: 'Production',
        action: {
            icon: {
                name: 'ico-plus'
            },
            onClick: () => { alert('add module...') }
        }
    },
    {
        label: 'Recipe',
        action: {
            icon: {
                name: 'ico-plus'
            },
            onClick: () => { alert('add module...') }
        }
    },
    {
        label: 'Vendors',
        action: {
            icon: {
                name: 'ico-plus'
            },
            onClick: () => { alert('add module...') }
        }
    }
];

/**
 * EchoHeader example
 * 
 * @name EchoHeaderExample
 * @component ../../EchoHeader.js
 */
export default class EchoHeaderExample extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentStore: 'SANFRAN #134Q',
            isHeaderVisible: false
        }

        this.toggleHeader = this.toggleHeader.bind(this);
    }
    
    switchStores() {
       alert('switch store context!');
    }

    toggleHeader() {
        this.setState({
            isHeaderVisible: !this.state.isHeaderVisible
        })
    }

    render () {
        return (
            <div>
                <EchoButton label="toggleHeader" onClick={this.toggleHeader} />
                {
                    this.state.isHeaderVisible
                        ? <EchoHeader context={this.state.currentStore} onClick={this.switchStores} navItems={navItems} accountItems={accountItems} />
                        : null
                }
            </div>
        );
    }
}