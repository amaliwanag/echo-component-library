import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import EchoHeader from './EchoHeader.js';

describe("A suite", function() {
    it("contains spec with an expectation", function() {
        expect(false).to.equal(true);
    });
    
    xit("contains spec with an expectation", function() {
        expect(shallow(<Foo />).is('.foo')).to.equal(true);
    });

    xit("contains spec with an expectation", function() {
        expect(mount(<Foo />).find('.foo').length).to.equal(1);
    });
});