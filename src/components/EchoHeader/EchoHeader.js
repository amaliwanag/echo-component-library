import React from 'react';
import styles from './EchoHeader.scss';

import { echoRowFactory } from '../EchoRow';
import { echoIconFactory } from '../EchoIcon';
import { echoDropDownFactory} from '../EchoDropDown';
import { echoDrawerFactory } from '../EchoDrawer';
import TopNav from './components/TopNav';
import SubNav from './components/SubNav';

const EchoIcon = echoIconFactory.getClass();
const EchoRow = echoRowFactory.getClass();
const EchoDropDown = echoDropDownFactory.getClass();
const EchoDrawer = echoDrawerFactory.getClass();

/**
 * Enter description.
 * 
 * @name EchoHeader
 */
class EchoHeader extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isDrawerOpen: false
        }

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({isDrawerOpen: !this.state.isDrawerOpen});
    }
    

    updateBodyPadding() {
        if (window.innerWidth <= 1024) {
            document.body.style.paddingTop = '56px';
        } else {
            document.body.style.paddingTop = '104px';
        }
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.accountItems) != JSON.stringify(nextProps.accountItems)) {
            this.setState({
                accountItems: nextProps.accountItems
            })
        }

        if (JSON.stringify(nextProps.navItems) != JSON.stringify(nextProps.navItems)) {
            this.setState({
                navItems: nextProps.navItems
            })
        }
    }

    componentWillMount() {
        this.setState({
            navItems: this.props.navItems,
            accountItems: this.props.accountItems
        })
    }

    componentDidMount() {
        if (window.innerWidth <= 1024) {
            document.body.style.paddingTop = '56px';
        } else {
            document.body.style.paddingTop = '104px';
        }

        window.addEventListener("resize", this.updateBodyPadding);
    }

    componentWillUnmount() {
        document.body.style.paddingTop = '0px';
        window.removeEventListener("resize", this.updateBodyPadding);
    }

    render() {
        if (this.theme) {
            //do something with theme
        }

		return (
            <div className={styles.headerContainer}>
                <div className={styles.header}>
                    <EchoRow justify="space-between" noPadding>
                        <div>
                            <div className={styles.appIcon}>
                                <EchoIcon name="ico-grid" height={40} color="white" onClick={this.handleClick} />
                                <EchoDrawer isOpen={this.state.isDrawerOpen} onClick={this.handleClick} navItems={this.state.navItems} accountItems={this.state.accountItems} />
                            </div>
                            <div className={styles.brand}>
                                <EchoIcon style={{marginRight: 15}} name="ico-hslogo_mono" width={120} height={30} color="white" />
                            </div>
                        </div>
                        <div onClick={this.props.onClick}>
                            { this.props.context }
                            <EchoIcon name="ico-chevron-down-sm" width={30} height={30} color="white" />
                        </div>
                        <TopNav items={this.state.accountItems.slice(0,3)} />
                    </EchoRow>
                </div>
                <div className={styles.subHeader}>
                    <EchoRow justify="flex-start">
                        <div className={styles.subAppIcon}>
                            <EchoDropDown items={this.state.navItems}>
                                <EchoIcon name="ico-grid" height={35} color="$gray" />
                            </EchoDropDown>
                        </div>
                        <div className={styles.divider}></div>
                        <SubNav items={this.state.navItems.slice(0,5)} />
                    </EchoRow>
                </div>
            </div>
        );

    }

}

EchoHeader.defaultProps = {

};

EchoHeader.propTypes = {
     /**
     * The collection of objects that will create the navigation hierarchy. Visiblie in the app drawer 
     * and the sub-header. Object properties are label, onClick, action, icon, template, and children.  Label is the string to 
     * be displayed on the dropdown list item. OnClick is the function to be called when
     * the item is clicked.  Action is an object that defines a secondary action available
     * to that line item; it takes an onClick property and a icon property. Icon is an icon 
     * object that will display an icon before the item label. Template takes a string which 
     * corresponds to a predifined template or a function that returns JSX. Predefined templates
     * include 'section-break'. Children is an array of sub-item objects.
     * 
     */
    navItems: React.PropTypes.array.isRequired,
    /**
     * Same as navItems. But will be displayed in the lower
     * section of the drawer and in the upper right of the header. Used for options concerning 
     * the user's settings.  
     * 
     */
    accountItems: React.PropTypes.array,
    /**
     * Text to be displayed in the middle of the header.
     * Generally used to display the store context. 
     * 
     */
    context: React.PropTypes.string,
    /**
     * Function to be called when the context is clicked.
     * Generally used to a display a modal for navigating between stores. 
     * 
     */
    onClick: React.PropTypes.func
};

export default EchoHeader;