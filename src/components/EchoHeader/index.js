import React from 'react';
import ReactDOM from 'react-dom';
import EchoHeader from './EchoHeader.js';

class EchoHeaderFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoHeader.prototype.theme = this.theme;
    return EchoHeader;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoHeader {...props} />, document.querySelector(selector));
  }
}

export let echoHeaderFactory = new EchoHeaderFactory();