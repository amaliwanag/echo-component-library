import React from 'react';
import styles from './index.scss';

//sub-components
import { echoRowFactory } from '~/src/components/EchoRow';
import { echoIconFactory } from '~/src/components/EchoIcon';

const EchoRow   = echoRowFactory.getClass();
const EchoIcon  = echoIconFactory.getClass();

const TopNav = ({items}) => {
    return (
        <div className={styles.topNav}>
            {
                items.map((item, index)=> {
                    return  <span key={index} style={{padding: '0 2px 0 2px'}}>
                                <EchoIcon name={item.icon && item.icon.name} color="white" />
                            </span>
                })
            }
        </div>
    );
};

export default TopNav;