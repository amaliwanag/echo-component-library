import React from 'react';
import styles from './index.scss';

//sub-components
import { echoRowFactory } from '~/src/components/EchoRow';
import { echoIconFactory } from '~/src/components/EchoIcon';

const EchoRow   = echoRowFactory.getClass();
const EchoIcon  = echoIconFactory.getClass();

const SubNav = ({items}) => {
    console.log('re-rendering');
    return (
        <ul className={styles.subNav}>
            {
                items.map((item, index)=> {

                    
                    return  <li key={index} onClick={item.onClick && item.onClick.bind(null, item)}>
                                { item.label }
                                <div className={item.isActive ? styles.active:null}></div>
                            </li>
                })
            }
        </ul>
    );
};

export default SubNav;