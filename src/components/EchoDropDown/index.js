import React from 'react';
import ReactDOM from 'react-dom';
import EchoDropDown from './EchoDropDown.js';

class EchoDropDownFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoDropDown.prototype.theme = this.theme;
    return EchoDropDown;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoDropDown {...props} />, document.querySelector(selector));
  }
}

export let echoDropDownFactory = new EchoDropDownFactory();