import React from 'react';
import { echoDropDownFactory } from '../../EchoDropDown';
import { echoButtonFactory } from '../../EchoButton';
import { echoRowFactory } from '../../EchoRow';

let EchoDropDown =  echoDropDownFactory.getClass();
let EchoButton  = echoButtonFactory.getClass();
let EchoRow  = echoRowFactory.getClass();


const items = [
    {
        label: 'item one',
        onClick: () => { alert('Navigating...')}
    },
    {
        label: 'this is an item with a longer name',
        children: [
            {
                label: 'sub-item one'
            },
            {
                label: 'sub-item two',
                children: [
                    {
                        label: 'sub-item three'
                    },
                    {
                        label: 'sub-item four',
                        children: [
                            {
                                label: 'sub-item five'
                            },
                            {
                                label: 'sub-item six'
                            }
                        ]
                    }
                ]
            }
        ]
    }
];

/**
 * EchoDropDown example
 * 
 * @name EchoDropDownExample
 * @component ../../EchoDropDown.js
 */
export default class EchoDropDownExample extends React.Component {
    render () {
        return (
            <EchoRow justify="space-between" noPadding>
                <EchoDropDown items={items} justify="left">
                    <EchoButton label="dropdown left"/>
                </EchoDropDown>
                <EchoDropDown items={items} justify="right" offset={10}>
                    <EchoButton label="dropdown right"/>
                </EchoDropDown>
            </EchoRow>
        );
    }
}