import React from 'react';
import Styles from './EchoDropDown.scss';

//animations
import makeFade from '~/src/containers/animations/makeFade.js';

//child components
import DropdownMenu from './components/DropdownMenu';
const FadingMenu = makeFade(DropdownMenu, {timeout: 10, duration: 0.15});

/**
 * Enter description.
 * 
 * @name EchoDropDown
 */
class EchoDropDown extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };

        this.handleClick        = this.handleClick.bind(this);
        this.handleBlur         = this.handleBlur.bind(this);
    }

    handleClick(e) {
        this.setState({ isOpen: true });
    }

    handleBlur(e) {
        const _this = this;

        if (this.list) {
            setTimeout(function () {
                _this.setState({ isOpen: false });
            }, 200);
        }
    }

    componentDidUpdate() {
        if (this.list) {
            this.list.focus();
        }
    }

    render() {
        let style = {
            left: 0 + this.props.offset
        }

        if (this.props.justify === 'right') {
            style = { right: 0 + this.props.offset }
        } 

        return (
            <div className={Styles.dropdownContainer}>
                <label onClick={this.handleClick} ref={(elem) => { this.label = elem }}>
                    {this.props.children}
                </label>
                {
                    this.state.isOpen
                        ? <div  tabIndex="0"
                                style={style} 
                                ref={(elem) => { this.list = elem }}
                                style={style}
                                onBlur={this.handleBlur}
                                className={Styles.dropDownMenu}>
                            <FadingMenu items={this.props.items} justify={this.props.justify}  />
                          </div>
                        : null
                }
            </div>
        );

    }

}

EchoDropDown.defaultProps = {
    justify: 'left',
    offset: 0
};

EchoDropDown.propTypes = {
    /**
     * The collection of objects that will create the dropdown structure. Object
     * properties are label, onClick, action, and children.  Label is the string to 
     * be displayed on the dropdown list item. OnClick is the function to be called when
     * the item is clicked.  Action is an object that defines a secondary action available
     * to that line item; it takes an onClick property and a icon property (see EchoTree). 
     * Children is an array of sub-item objects.
     * 
     */
    items: React.PropTypes.array.isRequired,
    /**
     * The dropdown will align to the left or right of its trigger
     * element. Defaults to left.
     */
    justify: React.PropTypes.oneOf(['left', 'right']),
    /**
     * This is the number of pixels that gets added to the left or right alignment.
     * Defaults to 0.
     */
    offset: React.PropTypes.number
};

export default EchoDropDown;