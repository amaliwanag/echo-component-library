import React from 'react';
import styles from './index.scss';

//children
import { echoTreeFactory } from '~/src/components/EchoTree';
const EchoTree = echoTreeFactory.getClass();

const DropdownMenu = ({items, justify, isAnimated, onAnimate, animationStyles}) => {
    let style = {};

    if (animationStyles && onAnimate) {
        style = Object.assign(style, animationStyles.start);
        style = isAnimated ? Object.assign(style, animationStyles.end):style;
    }

    return (
        <div style={style} className={styles.dropdownMenu}>
            <div className={`echo-caret-up ${justify}`}></div>
            <EchoTree collection={items} />
        </div>
    );
};

export default DropdownMenu;