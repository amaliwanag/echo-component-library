import React from 'react';
import { echoTextFactory } from '../../EchoText';

let EchoText =  echoTextFactory.getClass();

/**
 * EchoText example
 * 
 * @name EchoTextExample
 * @component ../../EchoText.js
 */
export default class EchoTextExample extends React.Component {
    render () {
        return (
            <div>
                <EchoText>
                    heyy
                </ EchoText>
                <EchoText italic>
                    heyy
                </ EchoText>
                <EchoText bold color="blue">
                    heyy
                </ EchoText>
                <EchoText thin uppercase>
                    heyy
                </ EchoText>
                <EchoText thin uppercase>
                    heyy
                </ EchoText>
                <EchoText size="tiny">
                    heyy
                </ EchoText>
            </div>
        );
    }
}