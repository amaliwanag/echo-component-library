import React from 'react';
import ReactDOM from 'react-dom';
import EchoText from './EchoText.js';

class EchoTextFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoText.prototype.theme = this.theme;
    return EchoText;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      console.log('props', props);
      ReactDOM.render(<EchoText {...props} />, document.querySelector(selector));
  }
}

export let echoTextFactory = new EchoTextFactory();