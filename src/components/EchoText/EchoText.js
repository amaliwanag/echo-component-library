import React from 'react';
import Styles from './EchoText.scss';
import classnames from 'classnames';

const classes = classnames.bind(Styles);

/**
 * Enter description.
 * 
 * @name EchoText
 */
class EchoText extends React.Component {
	render() {
		const { bold, center, children, color, italic, size, uppercase, lowercase, thin } = this.props;
		return(
			<div className={
				classes(Styles.hsText, Styles[color], Styles[size], {
					[Styles.bold]: bold,
					[Styles.center]: center,
					[Styles.italic]: italic,
					[Styles.uppercase]: uppercase,
					[Styles.lowercase]: lowercase,
					[Styles.thin]: thin
				})
			}>
				{children}
			</div>
		);
	}
}

EchoText.propTypes = {
    /**
     * Centers text if true.
     */
	center: React.PropTypes.bool,
    /**
     * Text elements. 
     */
	children: React.PropTypes.any,
    /**
     * Makes text bold if true.
     */
	bold: React.PropTypes.bool,
    /**
     * Takes a sass color name. Will make text that color.
     */
	color: React.PropTypes.string,
    /**
     * Makes text italic if true.
     */
	italic: React.PropTypes.bool,
    /**
     * Controls font size.
     */
	size: React.PropTypes.oneOf(['tiny', 'small', 'medium', 'h4', 'h3', 'h2', 'h1', 'hero']),
    /**
     * Transforms text to uppercase if true.
     */
	uppercase: React.PropTypes.bool,
    /**
     * Transforms text to lowercase if true.
     */
	lowercase: React.PropTypes.bool,
    /**
     * Makes font weight 300 if true.
     */
	thin: React.PropTypes.bool
};

EchoText.defaultProps = {
	bold: false,
	italic: false,
	thin: false
};

export default EchoText;