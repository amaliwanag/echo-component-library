import React from 'react';
import ReactDOM from 'react-dom';

// Components
import EchoModal from './Modal.component.js';

class ModalFactory {
  constructor() {
  }

  setTheme(theme) {
    this.theme = theme;
  }
  
  getClass() {
    EchoModal.prototype.theme = this.theme;
    return EchoModal;
  }

  render(selector, props) {
      Object.assign(props, this.theme);
      ReactDOM.render(<EchoModal {...props} />, document.querySelector(selector));
  }
}

export let modalFactory = new ModalFactory();