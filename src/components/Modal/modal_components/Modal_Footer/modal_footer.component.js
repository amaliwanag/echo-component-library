import React from 'react';
import ButtonComponent from '../../../Button/EchoButton.js';
import styles from './_modal_footer.scss';

const ModalFooterComponent = ( {footerClassName, confirm_text, cancel_text, handleClickConfirm, handleClickCancel, theme} ) => {


	return (
		<div className={`${footerClassName} ${styles.modalFooter}`}>
			<ButtonComponent 
				buttonClassName={styles.confirmButton}
				onClick={handleClickConfirm}
				label={confirm_text}
				theme={theme}>
			</ButtonComponent>
			<ButtonComponent 
				buttonClassName={styles.cancelButton}
				onClick={handleClickCancel}
				label={cancel_text}
				theme={theme}>
			</ButtonComponent>
		</div>

	);

};

ModalFooterComponent.propTypes = {
	confirm_text		: React.PropTypes.string,
	cancel_text			: React.PropTypes.string,
	handleClickConfirm	: React.PropTypes.func,
	handleClickCancel	: React.PropTypes.func,
	theme				: React.PropTypes.object
};

export default ModalFooterComponent;