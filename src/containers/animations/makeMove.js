import React, { Component } from 'react';

const MakeMove = (Target, options = {duration: 0.5}) => {

    const startStyle = {
        transition: `transform ${options.duration || 0.5}s linear`,
        transform: `translate(${options.xStart || 0}, ${options.yStart || 0})`
    };

    const endStyle = {
        transform: `translate(${options.xEnd || 0}, ${options.yEnd || 0})`
    };

    return class extends Component {
        constructor(props) {
            super(props);
            this.state = { isAnimated: false };

            this.handleAnimate = this.handleAnimate.bind(this);
            this.apply = this.apply.bind(this);
            this.remove = this.remove.bind(this);
        }

        handleAnimate(e) {
            this.setState({ isAnimated: !this.state.isAnimated });
        }

        apply() {
            this.setState({ isAnimated: true});
        }

        remove() {
            this.setState({ isAnimated: false });
        }

        componentWillReceiveProps(nextProps) {
            if (nextProps.isAnimated != this.state.isAnimated) {
                nextProps.isAnimated ? this.apply() : this.remove();
            }
        }

        componentDidMount() {
            const _this = this;

            if (options.timeout) {
                setTimeout(function() {
                    _this.handleAnimate();
                }, options.timeout);
            }
        }

        render() {
            return (
                <Target {...this.props}
                    isAnimated={this.state.isAnimated}
                    onAnimate={this.handleAnimate}
                    animationStyles={{ start: startStyle, end: endStyle }} />
            );
        }
    }
};

export default MakeMove;