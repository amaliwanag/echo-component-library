import React, { Component } from 'react';


const MakeRotate = (Target, options) => {
    const startStyle = {
        transition: `transform ${options.duration}s linear`,
        transform: 'rotate(0deg)'
    };

    const endStyle = {
        transform: `rotate(${options.degrees}deg)`
    };

    return class extends Component {
        constructor(props) {
            super(props);
            this.state = { isAnimated: false };
            this.handleAnimate = this.handleAnimate.bind(this);
        }

        handleAnimate(e) {
            this.setState({ isAnimated: !this.state.isAnimated });
        }

         //for use when animation needs to be controlled externally
        componentWillReceiveProps(nextProps) {
            if (nextProps.isAnimated != this.state.isAnimated) {
                this.handleAnimate();
            }
        }

        render() {
            return (
                <Target {...this.props}
                    isAnimated={this.state.isAnimated}
                    onAnimate={this.handleAnimate}
                    animationStyles={{ start: startStyle, end: endStyle }} />
            );
        }
    }
};

export default MakeRotate;