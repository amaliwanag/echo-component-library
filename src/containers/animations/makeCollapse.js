import React, { Component } from 'react';
import Collapse from 'react-collapse';

const MakeCollapse = (Target, options = {duration: 0.5}) => {
    return class extends Component {
        constructor(props) {
            super(props);
            this.state = { isAnimated: false };

            this.handleAnimate = this.handleAnimate.bind(this);
        }

        handleAnimate(e) {
            this.setState({ isAnimated: !this.state.isAnimated });
        }

        //for use when animation needs to be controlled externally
        componentWillReceiveProps(nextProps) {
            if (nextProps.isAnimated != this.state.isAnimated) {
                this.handleAnimate();
            }
        }

        render() {
            return (
                <Collapse isOpened={this.state.isAnimated} hasNestedCollapse>
                    <Target {...this.props}
                        isAnimated={this.state.isAnimated}
                        onAnimate={this.handleAnimate}
                    />
                </Collapse>
            );
        }
    }
};

export default MakeCollapse;