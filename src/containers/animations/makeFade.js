import React, { Component } from 'react';

const MakeFade = (Target, options = {duration: 0.5}) => {

    const startStyle = {
        opacity: 0,
        visibility: 'hidden',
        transition: `opacity ${options.duration || 0.5}s linear, visibility 0.5s`
    };

    const endStyle = {
        visibility: 'visible',
        opacity: options.endOpacity || 1
    };

    return class extends Component {
        constructor(props) {
            super(props);
            this.state = { isAnimated: false };

            this.handleAnimate = this.handleAnimate.bind(this);
            this.apply = this.apply.bind(this);
            this.remove = this.remove.bind(this);
        }

        handleAnimate(e) {
            this.setState({ isAnimated: !this.state.isAnimated });
        }

        apply() {
            this.setState({ isAnimated: true });
        }

        remove() {
            this.setState({ isAnimated: false });
        }

        //for use when animation needs to be controlled externally
        componentWillReceiveProps(nextProps) {
            if (nextProps.isAnimated != this.state.isAnimated) {
                nextProps.isAnimated ? this.apply() : this.remove();
            }
        }

        //for use when animation needs to be triggered immediately
        componentDidMount() {
            const _this = this;

            if (options.timeout) {
                setTimeout(function() {
                    _this.handleAnimate();
                }, options.timeout);
            }
        }

        render() {
            return (
                <Target {...this.props}
                    isAnimated={this.state.isAnimated}
                    onAnimate={this.handleAnimate}
                    animationStyles={{ start: startStyle, end: endStyle }} />
            );
        }
    }
};

export default MakeFade;